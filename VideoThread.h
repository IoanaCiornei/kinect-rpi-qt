#pragma once

#include "MyFreenectDevice.h"
#include "TargetCollection.h"
#include <QThread>
#include <QImage>
#include <QRect>

class VideoThread : public QThread
{
	Q_OBJECT;

private:
	Freenect::Freenect freenect;
	MyFreenectDevice*  device;
	TargetCollection   targets;
	int idTargetsType;

	QImage doCompositing(const QImage&, const QImage&);
	void overlayTarget(const Target&, QPainter&, const QColor&,
	                   const QImage&, const QImage&);

signals:
	void frameReady(QImage);
	void updateTargets(QObject*);

public slots:
	void newPhotogRect(QRect);
	void newSubjectRect(QRect);
	void deleteSubjectRect(QRect);
	void clearSelections();

public:
	void run();
	VideoThread();
	~VideoThread();
	void connectToDevice();
	void stopDevice();
};

