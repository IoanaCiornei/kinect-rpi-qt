#ifndef _BLUETOOTH_COMMUNICATOR_H_
#define _BLUETOOTH_COMMUNICATOR_H_

#include <QObject>
#include <QTextStream>

class BluetoothCommunicator : public QObject
{
	Q_OBJECT;

public:
	BluetoothCommunicator(int channel);
	int connectToDevice();

	QString deviceFilename;
	int btDevice;
	int channel;

signals:
	void updateStatus(int);

public slots:
	void sendWarning(int);
	void updatePanel(int bright, int warm, int cold);
	void getConnectionStatus();
	void updateBluetoothHandle();
	void closeApplication();
private:
	void updateBright(int value);
};

#endif /* _BLUETOOTH_MANAGER_H_ */
