#include "MouseArea.h"

#include <QDebug>

MouseArea::MouseArea(const QString& text, QWidget* parent)
                   : QLabel(parent)
{
	this->selectionMode = MA_SELECT_NONE;
	selecting = false;
	setText(text);
	this->resize(639, 479);
}

MouseArea::~MouseArea()
{

}

MouseArea::MouseArea(QWidget* parent) : QLabel(parent)
{
	this->selectionMode = MA_SELECT_NONE;
	selecting = false;
}

void MouseArea::truncateSelection(mouseRect& m_selection)
{
	int size_x = 638; //this->width() - 1;
	int size_y = 478; //this->height() - 1;
	if (m_selection.first.x < 0)
		m_selection.first.x = 0;
	if (m_selection.first.x > size_x)
		m_selection.first.x = size_x;
	if (m_selection.first.y < 0)
		m_selection.first.y = 0;
	if (m_selection.first.y > size_y)
		m_selection.first.y = size_y;
	if (m_selection.second.x < 0)
		m_selection.second.x = 0;
	if (m_selection.second.x > size_x)
		m_selection.second.x = size_x;
	if (m_selection.second.y < 0)
		m_selection.second.y = 0;
	if (m_selection.second.y > size_y)
		m_selection.second.y = size_y;
}

void MouseArea::mouseMoveEvent(QMouseEvent *ev)
{
	maSelectionMode selectionMode;

	selectionModeMutex.lock();
	selectionMode = this->selectionMode;
	selectionModeMutex.unlock();

	if (selectionMode == MA_SELECT_NONE)
		return;

	selectionMutex.lock();
	m_selection.second.x = ev->pos().x();
	m_selection.second.y = ev->pos().y();
	truncateSelection(m_selection);
	printf("mouseMove: %d, %d\n",
	       m_selection.second.x,
	       m_selection.second.y);
	selectionMutex.unlock();
}

void MouseArea::leaveEvent(QEvent *ev)
{
	maSelectionMode selectionMode;

	selectionModeMutex.lock();
	selectionMode = this->selectionMode;
	selectionModeMutex.unlock();

	if (selectionMode == MA_SELECT_NONE)
		return;
}

void MouseArea::mousePressEvent(QMouseEvent *ev)
{
	maSelectionMode selectionMode;

	selectionModeMutex.lock();
	selectionMode = this->selectionMode;
	selectionModeMutex.unlock();

	if (selectionMode == MA_SELECT_NONE)
		return;

	selectionMutex.lock();
	selecting = true;
	m_selection.first.x = ev->pos().x();
	m_selection.first.y = ev->pos().y();
	m_selection.second = m_selection.first;
	truncateSelection(m_selection);
	selectionMutex.unlock();
}

void MouseArea::mouseReleaseEvent(QMouseEvent *ev)
{
	AppUtils *utils = AppUtils::inst();
	maSelectionMode selectionMode;
	QRect rect;

	selectionModeMutex.lock();
	selectionMode = this->selectionMode;
	selectionModeMutex.unlock();

	if (selectionMode == MA_SELECT_NONE)
		return;

	selectionMutex.lock();
	selecting = false;

	m_selection.second.x = ev->pos().x();
	m_selection.second.y = ev->pos().y();
	truncateSelection(m_selection);
	qDebug("rect from (%d,%d) to (%d,%d)\n",
	       m_selection.first.x,
	       m_selection.first.y,
	       m_selection.second.x,
	       m_selection.second.y);
	rect = utils->convertToQRect(m_selection);
	selectionMutex.unlock();

	if (selectionMode == MA_SELECT_PHOTOG) {
		emit newPhotogRect(rect);
	} else if (selectionMode == MA_SELECT_SUBJECT) {
		emit newSubjectRect(rect);
	} else if (selectionMode == MA_DELETE_SUBJECT) {
		emit deleteSubjectRect(rect);
	} else {
		qDebug("wtf 2");
		exit(EXIT_FAILURE);
	}
}

bool MouseArea::tryGetSelection(QRect& selection)
{
	bool retval;
	AppUtils *utils = AppUtils::inst();

	selectionMutex.lock();
	if (selecting == true) {
		retval = true;
		selection = utils->convertToQRect(m_selection);
	} else {
		retval = false;
	}
	selectionMutex.unlock();
	return retval;
}

enum maSelectionMode MouseArea::getSelectionMode()
{
	return this->selectionMode;
}

void MouseArea::setSelectionMode(maSelectionMode selectionMode)
{
	selectionModeMutex.lock();
	this->selectionMode = selectionMode;
	selectionModeMutex.unlock();
}

