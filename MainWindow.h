#pragma once

#include <QMainWindow>
#include <QObject>
#include <opencv2/opencv.hpp>
#include <QString>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	Ui::MainWindow *ui;
	void doCompositing(QImage&);
	void closeEvent(QCloseEvent *event);
	void resetLabels(int exception);

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	Ui::MainWindow* getUI();

	QObject* getColdSlider();
	QObject* getWarmSlider();
	QObject* getBrightSlider();
	QObject* getWarningButton();
	QObject* getLabel();

signals:
	void signalUpdateBtPanel(int, int, int);
	void updateBluetoothHandle();
	void closeApplication();
	void sendWarning(int);
	void clearSelections();
	void startMotors();
	void stopMotors();

public slots:
	void button1_clicked();
	void button2_clicked();
	void button3_clicked();
	void button4_clicked();
	void button5_clicked();
	void buttonWarningPushed();
	void frameReady(QImage);
	void newPhotogRect(QRect);
	void newSubjectRect(QRect);
	void deleteSubjectRect(QRect);
	void updateStatus(int);
	void updateBtPanel();

private:
	void setPreview();
};

