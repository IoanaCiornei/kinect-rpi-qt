#pragma once

#include <opencv2/opencv.hpp>
#include <QString>
#include <QImage>
#include <QRect>

#define abs(x) (((x) > 0) ? (x) : -(x))

typedef std::pair<cv::Point, cv::Point> mouseRect;
enum maSelectionMode {
	MA_SELECT_PHOTOG,
	MA_SELECT_SUBJECT,
	MA_DELETE_SUBJECT,
	MA_SELECT_NONE
};

class AppUtils {
private:
	QVector<QRgb> grayTable;
public:
	QString BUTTON1_CAPTION_NORMAL;
	QString BUTTON1_CAPTION_PRESSED;
	QString BUTTON2_CAPTION_NORMAL;
	QString BUTTON2_CAPTION_PRESSED;
	QString BUTTON3_CAPTION_NORMAL;
	QString BUTTON3_CAPTION_PRESSED;
	QString BUTTON4_CAPTION_NORMAL;
	QString BUTTON4_CAPTION_PRESSED;
	QString BUTTON5_CAPTION_NORMAL;
	QString BUTTON5_CAPTION_PRESSED;
	QString FUZZY_IP;
	QString NO_CONNECTION;
	QString CMD_BRIGHT;
	QString CMD_WARM;
	QString CMD_COLD;
	QString CMD_WARNING;
	QString NO_PREVIEW_FILE;

	cv::Scalar RED;
	cv::Scalar GREEN;
	cv::Scalar BLUE;
	cv::Scalar BLACK;
	cv::Scalar WHITE;

	int FUZZY_PORT;
	int SCREEN_WIDTH;
	int SCREEN_HEIGHT;
	int WIDTH;
	int HEIGHT;
	int KEY_ESC;
	int CMD_MAX_LEN;
	int BLUETOOTH_CHANNEL;
	int CONNECTED;
	int NOT_CONNECTED;
	float TRAVEL_THRESHOLD;
	static AppUtils* inst();
	AppUtils();

	QImage convertToQImage(const cv::Mat& mat);
	QRect convertToQRect(const mouseRect& m_rect);
	bool rectOverlap(const QRect&, const QRect&);
	cv::Rect convertToCvRect(const QRect& rect);
	bool pointInside(const cv::Point&, const QRect&);
};

