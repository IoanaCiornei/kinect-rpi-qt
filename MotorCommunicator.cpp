#include "MotorCommunicator.h"
#include "AppUtils.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <QDebug>

int connectToFuzzy(QString ip, int port)
{
	struct sockaddr_in server;
	int serverSocket;
	int rc;

	/* create the socket */
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0) {
		qDebug("socket() returned exit code %d\n", errno);
		goto error;
	}

	/* prepare server info */
	server.sin_addr.s_addr = inet_addr(ip.toStdString().c_str());
	server.sin_family      = AF_INET;
	server.sin_port        = htons(port);

	/* connect to remote server */
	rc = connect(serverSocket, (struct sockaddr*) &server, sizeof(server));
	if (rc < 0) {
		fprintf(stderr, "connect() returned exit code %d\n", errno);
		goto error2;
	}
	return serverSocket;
error2:
	close(serverSocket);
error:
	return -errno;
}

MotorCommunicator::MotorCommunicator() : QObject()
{
	AppUtils *utils = AppUtils::inst();

	/* connecting to the raspberry */
	fuzzySocket = connectToFuzzy(utils->FUZZY_IP, utils->FUZZY_PORT);
	if (fuzzySocket < 0) {
		qDebug("connection failed!!!");
		exit(EXIT_FAILURE);
		return;
	}
	qDebug("Connected to fuzzy-rpi");
}

MotorCommunicator::~MotorCommunicator()
{
	close(fuzzySocket);
}

void MotorCommunicator::updateTargets(QObject *obj)
{
	AppUtils *utils = AppUtils::inst();
	TargetCollection* targets = (TargetCollection*) obj;
	std::vector<Target>& subjects = targets->getSubjects();
	std::vector<Target>::iterator subject;
	float leftmost, rightmost;
	char msg_buf[BUFSIZ];
	Target *photog;
	int rc;

	if (motorsEnabled == false)
		return;
	qDebug("updateTargets called");
	leftmost  = utils->WIDTH;
	rightmost = 0;
	for (subject = subjects.begin(); subject != subjects.end(); subject++) {
		qDebug("subject rect at (%d,%d)",
		       subject->rect.left(),
		       subject->rect.right()
		);
		if (leftmost  > subject->rect.left())
			leftmost  = subject->rect.left();
		if (rightmost < subject->rect.right())
			rightmost = subject->rect.right();
	}
	if (targets->hasPhotog()) {
		photog = targets->getPhotog();
		if (leftmost  > photog->rect.left())
			leftmost  = photog->rect.left();
		if (rightmost < photog->rect.right())
			rightmost = photog->rect.right();
	}
	leftmost  /= 320;
	rightmost /= 320;
	leftmost  -= 1.0;
	rightmost -= 1.0;

	sprintf(msg_buf, "phi 1 xleft %f xright %f\n", leftmost, rightmost);
	qDebug("sending \"%s\"", msg_buf);
	rc = send(fuzzySocket, msg_buf, strlen(msg_buf), 0);
	if (rc < 0) {
		qDebug("send() returned exit status %d", errno);
	}
}

void MotorCommunicator::startMotors()
{
	motorsEnabled = true;
}

void MotorCommunicator::stopMotors()
{
	char msg_buf[BUFSIZ];
	int rc;
	int tries = 30;

	motorsEnabled = false;
	usleep(500);
	memset(msg_buf, 0, BUFSIZ);
	sprintf(msg_buf, "theta 0\n");
	while (tries--) {
		/* FIXME: This is not a solution */
		rc = send(fuzzySocket, msg_buf, strlen(msg_buf), 0);
		rc = send(fuzzySocket, msg_buf, strlen(msg_buf), 0);
		rc = send(fuzzySocket, msg_buf, strlen(msg_buf), 0);
		rc = send(fuzzySocket, msg_buf, strlen(msg_buf), 0);
		if (rc < 0) {
			qDebug("send() returned exit status %d", errno);
			usleep(100);
			continue;
		}
		break;
	}
}

