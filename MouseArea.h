#ifndef MOUSEAREA_H
#define MOUSEAREA_H

#include <QLabel>
#include <QMouseEvent>
#include <QMutex>
#include "AppUtils.h"

class MouseArea : public QLabel
{
	Q_OBJECT;
	bool selecting;
	mouseRect m_selection;
	enum maSelectionMode selectionMode;
	QMutex selectionMutex;
	QMutex selectionModeMutex;

private:
	void truncateSelection(mouseRect&);

public:
	explicit MouseArea(const QString& text="", QWidget* parent=0);
	~MouseArea();

	MouseArea(QWidget *parent);

	void mouseMoveEvent(QMouseEvent *ev);
	void mousePressEvent(QMouseEvent *ev);
	void mouseReleaseEvent(QMouseEvent *ev);
	void leaveEvent(QEvent *ev);

	bool tryGetSelection(QRect& selection);

	enum maSelectionMode getSelectionMode();
	void setSelectionMode(maSelectionMode);

signals:
	void newPhotogRect(QRect);
	void newSubjectRect(QRect);
	void deleteSubjectRect(QRect);

public slots:
};

#endif // MOUSEAREA_H
