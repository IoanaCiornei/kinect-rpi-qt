#include "Processing.h"
#include "AppUtils.h"

void Processing::binarize(cv::Mat &image, std::pair<float, float> depthMinMax)
{
	cv::threshold(
		image,				 // src mat
		image,				 // dest mat?
		depthMinMax.first,	  // lowpass threshold
		255,				   // max binary value
		cv::THRESH_TOZERO	  // threshold type
	);
	cv::threshold(
		image,				 // src mat
		image,				 // dest mat?
		depthMinMax.second,	 // highpass threshold
		255,				   // max binary value
		cv::THRESH_TOZERO_INV  // threshold type
	);
}

/* Input: const cv::Mat& image
 * Output: pair of depth values: min and max corresponding to
 *         the largest depth cluster found
 * Returns: */
bool Processing::histogram(const cv::Mat &image, std::pair<float, float> &depthMinMax)
{
	const int   DEPTH_MIN = 1;
	const int   DEPTH_MAX = 256;
	const float DEPTH_SCALE = 5.0f;
	const int  channels[] = {0};
	const int  histSize[] = {(DEPTH_MAX - DEPTH_MIN) / DEPTH_SCALE};
	const float   range[] = {DEPTH_MIN, DEPTH_MAX};
	const float* ranges[] = {range};
	cv::MatND hist;
	int i;

	cv::calcHist(
		&image,    // image vector is just our image
		1,         // image vector size is 1
		channels,  // interested in the first channel only
		cv::Mat(), // no mask used
		hist,      // output array
		1,         // dims
		histSize,  // number of discrete quantization levels
		ranges,    // minimum and maximum value, 1 channel
		true,      // uniform
		false      // accumulate
	);
	bool newCluster = false;
	float cluster_weight;
	std::vector<float> cluster_weights;
	std::vector<std::pair<int, float> > cluster;
	std::vector<std::vector<std::pair<int, float> > > clusters;
	int pixels = image.rows * image.cols;

	for (i = 0; i < histSize[0]; i++) {
		float weight  = hist.at<float>(i);
		float percent = weight / pixels;
#ifdef VERBOSE
		std::cout<<weight<<" ";
#endif
		if (newCluster == true) {
			float cluster_start_depth;
			cluster_start_depth = cluster.at(0).first;
			if (percent <= 0.001f ||	// pixel weight falls under threshold
				i == histSize[0] - 1 || // end of histogram -> end of cluster
				i - cluster_start_depth > 0.2f * histSize[0]) // cluster already too long
			{
				newCluster = false;
				clusters.push_back(cluster);
				cluster_weights.push_back(cluster_weight);
			}
		}
		if (newCluster == false && percent >= 0.009f) {
			newCluster = true;
			cluster = std::vector<std::pair<int, float> >();
			cluster_weight = 0;
		}
		if (newCluster == true) {
			cluster.push_back(std::pair<int, float>(i, weight));
			cluster_weight += weight;
		}
	}
#ifdef VERBOSE
	std::cout<<std::endl;

	for (i = 0; i < clusters.size(); i++) {
		float weight = cluster_weights.at(i);
		std::cout<<"cluster "<<i<<" has total weight "<<weight<<std::endl;
		for (int j = 0; j < clusters.at(i).size(); j++) {
			std::cout<<"("<<clusters.at(i).at(j).first<<","<<clusters.at(i).at(j).second<<") ";
		}
		std::cout<<std::endl;
	}
#endif
	float max_weight   = -1;
	float total_weight = 0;
	int   max_cluster_index = -1;
	for (i = 0; i < clusters.size(); i++) {
		float weight = cluster_weights.at(i);
		if (max_weight < weight) {
			max_weight = weight;
			max_cluster_index = i;
		}
		total_weight += weight;
	}
	if (max_weight == -1)
		return false;
	std::vector<std::pair<int, float> > max_cluster;
	std::vector<std::pair<int, float> >::iterator pixel;
	float pixel_min_depth = 1000000;
	float pixel_max_depth = 0;
#ifdef VERBOSE
	std::cout<<"max_cluster_index is "<<max_cluster_index<<std::endl;
#endif
	max_cluster = clusters.at(max_cluster_index);
#ifdef VERBOSE
	std::cout<<"pixel depths: ";
#endif
	for (pixel = max_cluster.begin(); pixel != max_cluster.end(); pixel++) {
		float pixel_depth = DEPTH_MIN + DEPTH_SCALE * pixel->first;
#ifdef VERBOSE
		std::cout<<pixel_depth<<" ";
#endif
		if (pixel_min_depth > pixel_depth)
			pixel_min_depth = pixel_depth;
		if (pixel_max_depth < pixel_depth)
			pixel_max_depth = pixel_depth;
	}
#ifdef VERBOSE
	std::cout<<"\npixel depth for cluster "<<max_cluster_index<<" is between "
		<<pixel_min_depth<<" and "<<pixel_max_depth<<std::endl;
#endif
	depthMinMax.first  = pixel_min_depth;
	depthMinMax.second = pixel_max_depth;
	return true;
}

//QRect Processing::getBoundingRectangle(const std::vector<cv::Point2f>& features,
                                       //const std::vector<uchar>& status,
                                       //const QRect& oldRect, int& count)
//{
	//cv::Point2f featuresCenter(0, 0);
	 //FIXME: Hardcoded values 
	//QRect boundingRect(639, 479, 0, 0);
	//float minWidth, minHeight;
	//float x, y;

	//count = 0;
	//for (int i = 0; i < features.size(); i++) {
		//if (status[i]) {
			//x = features.at(i).x;
			//y = features.at(i).y;
			//if (boundingRect.left() > x)
				//boundingRect.setLeft(x);
			//if (boundingRect.top() > y)
				//boundingRect.setTop(y);
			//if (boundingRect.right() < x)
				//boundingRect.setRight(x);
			//if (boundingRect.bottom() < y)
				//boundingRect.setBottom(y);
			//count++;
		//}
	//}
	//featuresCenter.x = boundingRect.left() + (boundingRect.width() / 2);
	//featuresCenter.y = boundingRect.top() + (boundingRect.height() / 2);
	//minWidth  = oldRect.width();
	//minHeight = oldRect.height();
	//qDebug("center is at (%2.2f, %2.2f)", featuresCenter.x, featuresCenter.y);
	//qDebug("min size is (%2.2f, %2.2f)", minWidth, minHeight);
	//if (boundingRect.width() <= minWidth) {
		//boundingRect.setLeft(featuresCenter.x - minWidth / 2);
		//boundingRect.setWidth(minWidth);
	//}
	//if (boundingRect.height() <= minHeight) {
		//boundingRect.setTop(featuresCenter.y - minHeight / 2);
		//boundingRect.setHeight(minHeight);
	//}
	//if (boundingRect.left() < 0) {
		//boundingRect.setLeft(0);
	//}
	//if (boundingRect.top() < 0) {
		//boundingRect.setTop(0);
	//}
	//if (boundingRect.right() > 639) {
		//boundingRect.setRight(639);
	//}
	//if (boundingRect.bottom() > 479) {
		//boundingRect.setBottom(479);
	//}
	//return boundingRect;
//}

Target Processing::createDisplacedTarget(const std::vector<cv::Point2f>& prevFeatures,
                                         const std::vector<cv::Point2f>& nextFeatures,
                                         const std::vector<uchar>& status,
                                         const Target& prevTarget)
{
	int base_x = prevTarget.rect.left();
	int base_y = prevTarget.rect.top();
	cv::Point2f totalTravel = cv::Point2f(0, 0);
	cv::Point2f travel = cv::Point2f(0, 0);
	std::vector<cv::Point> correctedFeatures;
	AppUtils *utils = AppUtils::inst();
	QRect boundingRect;
	Target newTarget;

	for (int i = 0; i < prevFeatures.size(); i++) {
		if (status[i]) {
			qDebug("(%2.2f, %2.2f) <-> (%2.2f, %2.2f)",
				prevFeatures.at(i).x, prevFeatures.at(i).y,
				nextFeatures.at(i).x, nextFeatures.at(i).y);
			travel.x = nextFeatures.at(i).x - prevFeatures.at(i).x;
			travel.y = nextFeatures.at(i).y - prevFeatures.at(i).y;
			if (abs(travel.x) < utils->TRAVEL_THRESHOLD)
				travel.x = 0;
			if (abs(travel.y) < utils->TRAVEL_THRESHOLD)
				travel.y = 0;
			totalTravel.x += travel.x;
			totalTravel.y += travel.y;
			correctedFeatures.push_back(
					cv::Point(
						prevFeatures.at(i).x + travel.x,
						prevFeatures.at(i).y + travel.y
					)
				);
		}
	}
	if (correctedFeatures.size() == 0) {
		qDebug("qtf!!!");
		//exit(EXIT_FAILURE);
		newTarget.rect = prevTarget.rect;
		newTarget.locked = false;
		return newTarget;
	}
	totalTravel.x /= correctedFeatures.size();
	totalTravel.y /= correctedFeatures.size();
	qDebug("travel is (%2.2f, %2.2f), count is %lu",
			totalTravel.x,
			totalTravel.y,
			correctedFeatures.size()
	);

	boundingRect = QRect(
		base_x + travel.x,
		base_y + travel.y,
		prevTarget.rect.width(),
		prevTarget.rect.height()
	);
	if (boundingRect.left() < 0) {
		boundingRect.moveLeft(0);
	}
	if (boundingRect.top() < 0) {
		boundingRect.moveTop(0);
	}
	if (boundingRect.right() > 639) {
		boundingRect.moveRight(639);
	}
	if (boundingRect.bottom() > 479) {
		boundingRect.moveBottom(479);
	}
	newTarget = Target(boundingRect);
	for (int i = 0; i < correctedFeatures.size(); i++) {
		cv::Point relative(
				correctedFeatures.at(i).x - boundingRect.left(),
				correctedFeatures.at(i).y - boundingRect.top()
		);
		if (utils->pointInside(correctedFeatures.at(i), boundingRect) == true)
			newTarget.features.push_back(relative);
	}
	newTarget.locked = (newTarget.features.size() >= 2);
	return newTarget;
}

/* FIXME: CONST prevTarget */
Target Processing::track(Target&  prevTarget,
                         const cv::Mat& prevImage,
                         const cv::Mat& image)
{
	int base_x = prevTarget.rect.left();
	int base_y = prevTarget.rect.top();
	std::vector<cv::Point2f> prevFeatures;
	std::vector<cv::Point2f> nextFeatures;
	std::vector<uchar> status;
	std::vector<float> err;
	std::vector<cv::Point>::iterator f;
	std::vector<cv::Point> prevRelativeFeatures = prevTarget.getFeatures();
	cv::Mat prevGrayImage;
	cv::Mat grayImage;
	Target newTarget;
	int trackedFeatureCount;

	cv::cvtColor(prevImage, prevGrayImage, CV_BGR2GRAY);
	cv::cvtColor(image, grayImage, CV_BGR2GRAY);

	if (prevTarget.getDepthMask().empty()) {
		qDebug("depth mask empty!!!");
		prevTarget.locked = false;
		return prevTarget;
	}
	if (prevTarget.getFeatures().empty()) {
		qDebug("prev features empty!!");
		prevTarget.locked = false;
		return prevTarget;
	}

	for (f = prevRelativeFeatures.begin(); f != prevRelativeFeatures.end(); f++) {
		prevFeatures.push_back(cv::Point2f(f->x + base_x, f->y + base_y));
	}

	cv::calcOpticalFlowPyrLK(
		prevGrayImage,
		grayImage,
		prevFeatures,
		nextFeatures,
		status,
		err
	);
	qDebug("prevTarget.rect is (%d, %d, %d, %d)",
			prevTarget.rect.top(), prevTarget.rect.left(),
			prevTarget.rect.height(), prevTarget.rect.width());
	return createDisplacedTarget(
		prevFeatures,
		nextFeatures,
		status,
		prevTarget
	);
}

