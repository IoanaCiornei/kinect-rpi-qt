#pragma once

#include <opencv2/opencv.hpp>
#include <libfreenect.hpp>

class MyFreenectDevice : public Freenect::FreenectDevice {
private:
	bool               m_new_depth_frame;
	bool               m_new_rgb_frame;
	pthread_mutex_t    m_depth_mutex;
	pthread_mutex_t    m_rgb_mutex;
	cv::Mat            depthMat;
	cv::Mat            rgbMat;

	void VideoCallback(void* _rgb, uint32_t timestamp);
	void DepthCallback(void* _depth, uint32_t timestamp);

public:
	MyFreenectDevice(freenect_context *_ctx, int _index);
	bool getVideo(cv::Mat& output);
	bool getDepth(cv::Mat& output);
};
