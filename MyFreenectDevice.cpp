#include "MyFreenectDevice.h"
#include "AppUtils.h"
#include <stdio.h>

MyFreenectDevice::MyFreenectDevice(freenect_context *_ctx, int _index)
                              : Freenect::FreenectDevice(_ctx, _index)
{
	AppUtils *utils = AppUtils::inst();

	depthMat = cv::Mat(cv::Size(utils->WIDTH, utils->HEIGHT), CV_16UC1, utils->WHITE);
	rgbMat   = cv::Mat(cv::Size(utils->WIDTH, utils->HEIGHT), CV_8UC3,  utils->WHITE);
	setDepthFormat(FREENECT_DEPTH_REGISTERED);
	setVideoFormat(FREENECT_VIDEO_RGB);
	pthread_mutex_init(&m_rgb_mutex,   NULL);
	pthread_mutex_init(&m_depth_mutex, NULL);
	m_new_rgb_frame   = false;
	m_new_depth_frame = false;
}

void MyFreenectDevice::VideoCallback(void* _rgb, uint32_t timestamp)
{
	uint8_t* rgb;
	pthread_mutex_lock(&m_rgb_mutex);
	rgb = static_cast<uint8_t*>(_rgb);
	rgbMat.data = rgb;
	m_new_rgb_frame = true;
	pthread_mutex_unlock(&m_rgb_mutex);
}

void MyFreenectDevice::DepthCallback(void* _depth, uint32_t timestamp)
{
	uint16_t* depth;
	pthread_mutex_lock(&m_depth_mutex);
	depth = static_cast<uint16_t*>(_depth);
	depthMat.data = (uchar*) depth;
	m_new_depth_frame = true;
	pthread_mutex_unlock(&m_depth_mutex);
}

bool MyFreenectDevice::getVideo(cv::Mat& output)
{
	bool retval(false);
	pthread_mutex_lock(&m_rgb_mutex);
	if (m_new_rgb_frame) {
		cv::cvtColor(rgbMat, output, CV_RGB2BGR);
		m_new_rgb_frame = false;
		retval = true;
	}
	pthread_mutex_unlock(&m_rgb_mutex);
	return retval;
}

bool MyFreenectDevice::getDepth(cv::Mat& output)
{
	bool retval(false);
	pthread_mutex_lock(&m_depth_mutex);
	if (m_new_depth_frame) {
		depthMat.copyTo(output);
		m_new_depth_frame = false;
		retval = true;
	}
	pthread_mutex_unlock(&m_depth_mutex);
	return retval;
}

