#include "VideoThread.h"
#include "AppUtils.h"
#include "Processing.h"
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QMetaType>
#include <QPainter>
#include <QDebug>

void VideoThread::connectToDevice()
{
	qDebug("Connecting to Kinect...");
	this->device = &this->freenect.createDevice<MyFreenectDevice>(0);
	device->startVideo();
	device->startDepth();
	qDebug("\tDone");
}

void VideoThread::stopDevice()
{
	device->stopVideo();
	device->stopDepth();
}

VideoThread::VideoThread()
{
}

VideoThread::~VideoThread()
{
}

void VideoThread::overlayTarget(const Target& target, QPainter& painter, const QColor& color,
                                const QImage& rgb, const QImage& depth)
{
	QImage overlayImage = depth.copy(target.rect);

	painter.setPen(QPen(color));
	painter.setOpacity(0.5);
	painter.setCompositionMode(QPainter::CompositionMode_Overlay);
	painter.drawImage(target.rect.left(), target.rect.top(), overlayImage);

	std::vector<cv::Point>::const_iterator point;
	for (point = target.features.begin(); point != target.features.end(); point++) {
		painter.fillRect(
			target.rect.x() + point->x,
			target.rect.y() + point->y,
			10, 10, color
		);
	}
	painter.drawRect(target.rect);
}

QImage VideoThread::doCompositing(const QImage& rgb, const QImage& depth)
{
	QImage image = QImage(rgb.size(), QImage::Format_ARGB32_Premultiplied);
	std::vector<Target>::iterator target;
	std::vector<Target>& subjects = targets.getSubjects();
	AppUtils *utils = AppUtils::inst();
	QPainter painter;

	painter.begin(&image);

	painter.setCompositionMode(QPainter::CompositionMode_Source);
	painter.fillRect(image.rect(), Qt::transparent);

	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
	painter.drawImage(0, 0, rgb);

	for (target = subjects.begin(); target != subjects.end(); target++) {
		overlayTarget(*target, painter, Qt::green, rgb, depth);
	}
	if (targets.hasPhotog()) {
		overlayTarget(*(targets.getPhotog()), painter, Qt::red, rgb, depth);
	}

	painter.end();
	return image;
}

void VideoThread::run()
{
	AppUtils *utils = AppUtils::inst();
	cv::Mat rgbMatPrev(
		cv::Size(utils->WIDTH, utils->HEIGHT),
		CV_8UC3,
		utils->WHITE
	);
	cv::Mat rgbMat(
		cv::Size(utils->WIDTH, utils->HEIGHT),
		CV_8UC3,
		utils->WHITE
	);
	cv::Mat depthMatRaw(
		cv::Size(utils->WIDTH, utils->HEIGHT),
		CV_16UC1,
		utils->WHITE
	);
	cv::Mat depthMat(
		cv::Size(utils->WIDTH, utils->HEIGHT),
		CV_8UC1,
		utils->WHITE
	);
	QImage image, rgbImage, depthImage;
	std::vector<Target> newSubjects;
	Target newSubject;
	Target newPhotog;
	bool update;

	while (true) {
		update  = false;
		update |= device->getVideo(rgbMat);
		update |= device->getDepth(depthMatRaw);

		if (update) {
			/* Scale 11 bit values to 8 bits */
			depthMatRaw.convertTo(depthMat, CV_8UC1, 255.0 / 2048.0);


			std::vector<Target>& subjects = targets.getSubjects();
			std::vector<Target>::iterator target;

			newSubjects.clear();
			for (target = subjects.begin(); target != subjects.end(); target++) {
				target->setDepthMask(depthMat);
				if (target->locked == false) {
					target->setFeatures(rgbMat);
					newSubjects.push_back(*target);
				} else {
					newSubject = Processing::track(*target, rgbMatPrev, rgbMat);
					newSubjects.push_back(newSubject);
				}
			}
			/* FIXME: RACE CONDITION */
			subjects = newSubjects;
			if (targets.hasPhotog()) {
				Target *photog = targets.getPhotog();
				photog->setDepthMask(depthMat);
				if (photog->locked == false) {
					photog->setFeatures(rgbMat);
				} else {
					/* FIXME: PRIVACY VIOLATION. BAD! */
					newPhotog = Processing::track(*photog, rgbMatPrev, rgbMat);
					targets.setPhotog(newPhotog);
				}
			}

			rgbImage   = utils->convertToQImage(rgbMat);
			depthImage = utils->convertToQImage(depthMat);
			image = doCompositing(rgbImage, depthImage);
			emit frameReady(image);
			emit updateTargets(&targets);
			rgbMat.copyTo(rgbMatPrev);
		}
	}
	device->stopVideo();
}

void VideoThread::newPhotogRect(QRect rect)
{
	targets.setPhotog(rect);
}

void VideoThread::newSubjectRect(QRect rect)
{
	targets.addSubject(rect);
}

void VideoThread::deleteSubjectRect(QRect rect)
{
	targets.removeSubjectsInRect(rect);
}

void VideoThread::clearSelections()
{
	targets.clearSubjects();
}

