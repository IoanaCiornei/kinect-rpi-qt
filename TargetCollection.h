#pragma once

#include "Target.h"
#include <QObject>
#include <QMutex>
#include <QRect>

class TargetCollection : public QObject {
	Q_OBJECT;
	bool empty;
	std::vector<Target> subjects;
	Target *photog;
	QMutex mutex;

public:
	TargetCollection();
	std::vector<Target>& getSubjects();
	Target* getPhotog();
	void setPhotog(const QRect&);
	void setPhotog(const Target&);
	void addSubject(const QRect&);
	void removeSubjectsInRect(const QRect& rect);
	void clearSubjects();
	bool hasPhotog();
};

