#include <stdio.h>
#include "BluetoothCommunicator.h"
#include "AppUtils.h"

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <QFile>
#include <QTextStream>
#include <QProcess>
#include <QDebug>
#include <QThread>
#include "errno.h"

BluetoothCommunicator::BluetoothCommunicator(int channel) : QObject()
{
	this->deviceFilename.append(QString("/dev/rfcomm").append(QString::number(channel)));
	this->channel = channel;
	this->btDevice = connectToDevice();
}

int BluetoothCommunicator::connectToDevice()
{
	return open(deviceFilename.toStdString().c_str(), O_WRONLY);
}
void BluetoothCommunicator::sendWarning(int time)
{
	AppUtils *utils = AppUtils::inst();
	char buffer[utils->CMD_MAX_LEN];
	int num_bytes;

	sprintf(buffer, "%s %i\n\r", utils->CMD_WARNING.toStdString().c_str(), time);
	num_bytes = write(btDevice, buffer, strlen(buffer));
	printf("send cmd: %s\n", buffer);
}

void BluetoothCommunicator::updatePanel(int bright, int warm, int cold)
{
	updateBright(bright);
}

void BluetoothCommunicator::updateBright(int value)
{
	AppUtils *utils = AppUtils::inst();
	char command[utils->CMD_MAX_LEN];
	int num_bytes;

	sprintf(command, "%s %i\n\r", utils->CMD_BRIGHT.toStdString().c_str(), value);
	num_bytes = write(btDevice, command, strlen(command));
}

void BluetoothCommunicator::getConnectionStatus()
{
	QProcess process;
	QString command("rfcomm");
	QStringList params;
	AppUtils *utils;

	utils = AppUtils::inst();
	process.setProcessChannelMode(QProcess::MergedChannels);
	params << "show" << QString::number(channel);
	process.start(command, params);
	process.waitForFinished();
	process.waitForReadyRead();
	QString output = process.readAll();

	printf("%s\n", utils->NO_CONNECTION.toStdString().c_str());
	if (!QString::compare(output, utils->NO_CONNECTION, Qt::CaseSensitive)) {
		emit updateStatus(utils->NOT_CONNECTED);
	} else {
		if (btDevice != -1)
			emit updateStatus(utils->CONNECTED);
		else
			emit updateStatus(utils->NOT_CONNECTED);
	}
}

void BluetoothCommunicator::updateBluetoothHandle()
{
	printf("updating handle!!\n");
	if (btDevice == -1) {
		printf("it was -1\n");
		btDevice = connectToDevice();
		printf("now %i\n", btDevice);
	}
}

void BluetoothCommunicator::closeApplication()
{
	close(btDevice);
}
