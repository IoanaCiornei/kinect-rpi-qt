#include "TargetCollection.h"
#include "AppUtils.h"

TargetCollection::TargetCollection() : QObject()
{
	photog = NULL;
}

std::vector<Target>& TargetCollection::getSubjects()
{
	return subjects;
}

Target* TargetCollection::getPhotog()
{
	return photog;
}

void TargetCollection::setPhotog(const Target& target)
{
	mutex.lock();
	if (photog != NULL)
		delete(photog);
	photog = new Target(target);
	mutex.unlock();
}

void TargetCollection::setPhotog(const QRect& rect)
{
	mutex.lock();
	if (photog != NULL)
		delete(photog);
	photog = new Target(rect);
	mutex.unlock();
}

void TargetCollection::addSubject(const QRect& rect)
{
	mutex.lock();
	subjects.push_back(Target(rect));
	mutex.unlock();
}

void TargetCollection::removeSubjectsInRect(const QRect& rect)
{
	AppUtils *utils = AppUtils::inst();
	std::vector<Target>::iterator it;

	mutex.lock();
	for (it = subjects.begin(); it != subjects.end(); ) {
		if (utils->rectOverlap(it->rect, rect) == true) {
			it = subjects.erase(it);
		} else {
			it++;
		}
	}
	mutex.unlock();
}

void TargetCollection::clearSubjects()
{
	mutex.lock();
	subjects.clear();
	if (photog != NULL) {
		delete(photog);
		photog = NULL;
	}
	mutex.unlock();
}

bool TargetCollection::hasPhotog()
{
	bool answer;

	mutex.lock();
	answer = (photog != NULL);
	mutex.unlock();

	return answer;
}

