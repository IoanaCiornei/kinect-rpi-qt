#-------------------------------------------------
#
# Project created by QtCreator 2015-08-16T17:54:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kinect_rpi_qt
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    MyFreenectDevice.cpp \
    VideoThread.cpp \
    MouseArea.cpp

HEADERS  += MainWindow.h \
    MyFreenectDevice.h \
    AppUtils.h \
    kinect.h \
    VideoThread.h \
    MouseArea.h

FORMS    += MainWindow.ui
