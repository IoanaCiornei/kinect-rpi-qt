#pragma once

#include <opencv2/opencv.hpp>
#include "Target.h"

class Processing {
private:
	static Target createDisplacedTarget(
			const std::vector<cv::Point2f>& prevFeatures,
			const std::vector<cv::Point2f>& nextFeatures,
			const std::vector<uchar>& status,
			const Target& prevTarget);
public:
	static void binarize(cv::Mat&, std::pair<float, float>);
	static bool histogram(const cv::Mat&, std::pair<float, float>&);
	static Target track(Target& prevTarget,
                        const cv::Mat& prevImage,
                        const cv::Mat& image);
};

