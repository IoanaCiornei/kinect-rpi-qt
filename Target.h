#pragma once

#include <opencv2/opencv.hpp>
#include <QImage>
#include <QRect>

class Target {
public:
	QRect   rect;
	cv::Mat depthMask;
	std::vector<cv::Point> features;
	bool locked;

	Target();
	Target(const Target&);
	Target(QRect rect);
	std::vector<cv::Point> getFeatures();
	cv::Mat getDepthMask();
	void setFeatures(const cv::Mat&);
	void setDepthMask(const cv::Mat&);
};

