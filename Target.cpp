#include "Target.h"
#include "AppUtils.h"
#include "Processing.h"
#include <QDebug>

Target::Target()
{
	this->locked = false;
}

Target::Target(QRect rect)
{
	this->rect = rect;
	this->locked = false;
}

Target::Target(const Target& other)
{
	this->rect = other.rect;
	this->depthMask = other.depthMask;
	this->features = other.features;
	this->locked = other.locked;
}

void Target::setFeatures(const cv::Mat& rgb)
{
	/* FIXME move away from here */
	const int maxCorners = 20;
	const float qualityLevel = 0.6f;
	const float minDistance  = 40;

	AppUtils *utils = AppUtils::inst();
	if (rect.left() < 0 || rect.right() > 639 ||
		rect.top() < 0 || rect.bottom() > 479) {
		qDebug("qtf tree");
		exit(EXIT_FAILURE);
	}
	cv::Mat rgbCrop(rgb, utils->convertToCvRect(this->rect));
	cv::Mat grayCrop;

	cv::cvtColor(rgbCrop, grayCrop, CV_BGR2GRAY);

	qDebug("before goodFeaturesToTrack");
	goodFeaturesToTrack(
		grayCrop,
		this->features,   /* this parameter is OUTPUT */
		maxCorners,
		qualityLevel,
		minDistance,
		depthMask
	);
	qDebug("after goodFeaturesToTrack");
	this->locked = (this->features.size() >= 1);
}

std::vector<cv::Point> Target::getFeatures()
{
	return this->features;
}

void Target::setDepthMask(const cv::Mat& depth)
{
	qDebug("before setDepthMask");
	AppUtils *utils = AppUtils::inst();

	if (rect.left() < 0 || rect.right() > 639 ||
		rect.top() < 0 || rect.bottom() > 479) {
		qDebug("qtf salami");
		exit(EXIT_FAILURE);
	}
	/* Keeps min and max depth from mask */
	cv::Rect selRect = utils->convertToCvRect(this->rect);
	cv::Mat targetCrop(depth, selRect);
	std::pair<float, float> depthMinMax;

	Processing::histogram(targetCrop, depthMinMax);
	Processing::binarize(targetCrop,  depthMinMax);

	this->depthMask = targetCrop;
}

cv::Mat Target::getDepthMask()
{
	return this->depthMask;
}

