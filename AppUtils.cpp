#include "AppUtils.h"
#include <QDesktopWidget>
#include <QApplication>
#include <QDebug>
#include <stdio.h>

AppUtils* AppUtils::inst()
{
	static AppUtils* m_inst = NULL;
	if (m_inst == NULL) {
		m_inst = new AppUtils();
	}
	return m_inst;
}

AppUtils::AppUtils()
{
	QRect rec = QApplication::desktop()->screenGeometry();
	SCREEN_HEIGHT     = rec.height();
	SCREEN_WIDTH      = rec.width();
	WIDTH             = 640;
	HEIGHT            = 480;
	KEY_ESC           = 27;
	CMD_MAX_LEN       = 20;
	BLUETOOTH_CHANNEL = 0;
	CONNECTED         = 1;
	NOT_CONNECTED     = 2;
	TRAVEL_THRESHOLD  = 0.5f;
	RED           = cv::Scalar(128, 0,   0);
	GREEN         = cv::Scalar(0,   128, 0);
	BLUE          = cv::Scalar(0,   0,   128);
	BLACK         = cv::Scalar(0,   0,   0);
	WHITE         = cv::Scalar(255, 255, 255);

	FUZZY_IP      = "127.0.0.1";
	FUZZY_PORT    = 4232;

	BUTTON1_CAPTION_NORMAL  = "Select Photographer";
	BUTTON2_CAPTION_NORMAL  = "Add Subject";
	BUTTON3_CAPTION_NORMAL  = "Remove Subjects";
	BUTTON4_CAPTION_NORMAL  = "Clear Selections";
	BUTTON5_CAPTION_NORMAL  = "Start Motors";
	BUTTON1_CAPTION_PRESSED = "Cancel Selection";
	BUTTON2_CAPTION_PRESSED = "Cancel Selection";
	BUTTON3_CAPTION_PRESSED = "Cancel Selection";
	BUTTON4_CAPTION_PRESSED = "Clear Selections";
	BUTTON5_CAPTION_PRESSED = "Stop Motors";
	NO_CONNECTION           = "Get info failed: No such device\n";
	CMD_BRIGHT              = "bright";
	CMD_WARM                = "TODO";
	CMD_COLD                = "TODO";
	CMD_WARNING             = "warning";
	NO_PREVIEW_FILE         = "../preview.png";

	for (int i = 0; i < 256; i++) {
		grayTable.push_back(qRgb(i, i, i));
	}
}

QImage AppUtils::convertToQImage(const cv::Mat& mat)
{
	// Copy input Mat
	const uchar *qImageBuffer = (const uchar*) mat.data;

	// 8-bits unsigned, NO. OF CHANNELS=1
	if (mat.type() == CV_8UC1) {
		// Create QImage with same dimensions as input Mat
		QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
		img.setColorTable(grayTable);
		return img;
	}
	// 8-bits unsigned, NO. OF CHANNELS=3
	if (mat.type() == CV_8UC3) {
		// Create QImage with same dimensions as input Mat
		QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
		return img.rgbSwapped();
	} else {
		qDebug("ERROR: Mat could not be converted to QImage.");
		return QImage();
	}
}

bool AppUtils::rectOverlap(const QRect& r1, const QRect& r2)
{
	/* http://stackoverflow.com/questions/306316/determine-if-two-rectangles-overlap-each-other */
	if (r1.bottom() < r2.top()) {    /* r1 is to the top of r2 */
		return false;
	}
	if (r1.top()    > r2.bottom()) { /* r1 is to the bottom of r2 */
		return false;
	}
	if (r1.left()   > r2.right()) {  /* r1 is to the right of r2 */
		return false;
	}
	if (r1.right()  < r2.left()) {   /* r1 is to the left of r2 */
		return false;
	}
	return true;
}

QRect AppUtils::convertToQRect(const mouseRect& m_rect)
{
	QRect rect;

	rect.setTop(   cv::min(m_rect.first.y,
	                       m_rect.second.y));
	rect.setBottom(cv::max(m_rect.first.y,
	                       m_rect.second.y) - 1);
	rect.setLeft(  cv::min(m_rect.first.x,
	                       m_rect.second.x));
	rect.setRight( cv::max(m_rect.first.x,
	                       m_rect.second.x) - 1);
	return rect;
}

cv::Rect AppUtils::convertToCvRect(const QRect& rect)
{
	qDebug("creating rect of size (%d,%d) at (%d,%d)",
	       rect.width(), rect.height(), rect.left(), rect.top());
	return cv::Rect(
		rect.left(),
		rect.top(),
		rect.width(),
		rect.height()
	);
}

bool AppUtils::pointInside(const cv::Point& point, const QRect& rect)
{
	if (point.x < rect.left())
		return false;
	if (point.x > rect.right())
		return false;
	if (point.y < rect.top())
		return false;
	if (point.y > rect.bottom())
		return false;
	return true;
}
