#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include "ui_MainWindow.h"
#include "MainWindow.h"
#include "AppUtils.h"
#include <QMouseEvent>
#include <QPainter>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	AppUtils *utils = AppUtils::inst();

	ui->setupUi(this);
	resetLabels(-1);

	connect(ui->pushButton_1,  SIGNAL(clicked()),
	        this,              SLOT(button1_clicked()));
	connect(ui->pushButton_2,  SIGNAL(clicked()),
	        this,              SLOT(button2_clicked()));
	connect(ui->pushButton_3,  SIGNAL(clicked()),
	        this,              SLOT(button3_clicked()));
	connect(ui->pushButton_4,  SIGNAL(clicked()),
	        this,              SLOT(button4_clicked()));
	connect(ui->pushButton_5,  SIGNAL(clicked()),
	        this,              SLOT(button5_clicked()));
	connect(ui->warningButton, SIGNAL(clicked()),
            this,              SLOT(buttonWarningPushed()));
	connect(ui->label,         SIGNAL(newPhotogRect(QRect)),
	        this,              SLOT(newPhotogRect(QRect)));
	connect(ui->label,         SIGNAL(newSubjectRect(QRect)),
	        this,              SLOT(newSubjectRect(QRect)));
	connect(ui->label,         SIGNAL(deleteSubjectRect(QRect)),
	        this,              SLOT(deleteSubjectRect(QRect)));

	/* TODO */
	ui->btfeedback->setText("no connection");
	setPreview();
}

MainWindow::~MainWindow()
{
	delete ui;
}

QObject* MainWindow::getLabel()
{
	return ui->label;
}

void MainWindow::resetLabels(int exception)
{
	AppUtils* utils = AppUtils::inst();
	if (exception != 1)
		ui->pushButton_1->setText(utils->BUTTON1_CAPTION_NORMAL);
	if (exception != 2)
		ui->pushButton_2->setText(utils->BUTTON2_CAPTION_NORMAL);
	if (exception != 3)
		ui->pushButton_3->setText(utils->BUTTON3_CAPTION_NORMAL);
	if (exception != 4)
		ui->pushButton_4->setText(utils->BUTTON4_CAPTION_NORMAL);
	if (exception != 5)
		ui->pushButton_5->setText(utils->BUTTON5_CAPTION_NORMAL);
}

/* Select photographer */
void MainWindow::button1_clicked()
{
	cv::Mat img;
	AppUtils* utils = AppUtils::inst();

	resetLabels(1);
	if (ui->pushButton_1->text() == utils->BUTTON1_CAPTION_NORMAL) {
		ui->pushButton_1->setText(utils->BUTTON1_CAPTION_PRESSED);
		ui->label->setSelectionMode(MA_SELECT_PHOTOG);
	} else if (ui->pushButton_1->text() == utils->BUTTON1_CAPTION_PRESSED) {
		ui->pushButton_1->setText(utils->BUTTON1_CAPTION_NORMAL);
		ui->label->setSelectionMode(MA_SELECT_NONE);
	} else {
		qDebug("wtf");
	}
}

/* Add subject */
void MainWindow::button2_clicked()
{
	cv::Mat img;
	AppUtils* utils = AppUtils::inst();

	resetLabels(2);
	if (ui->pushButton_2->text() == utils->BUTTON2_CAPTION_NORMAL) {
		ui->pushButton_2->setText(utils->BUTTON2_CAPTION_PRESSED);
		ui->label->setSelectionMode(MA_SELECT_SUBJECT);
	} else if (ui->pushButton_2->text() == utils->BUTTON2_CAPTION_PRESSED) {
		ui->pushButton_2->setText(utils->BUTTON2_CAPTION_NORMAL);
		ui->label->setSelectionMode(MA_SELECT_NONE);
	} else {
		qDebug("wtf");
	}
}

/* Remove subjects */
void MainWindow::button3_clicked()
{
	cv::Mat img;
	AppUtils* utils = AppUtils::inst();

	resetLabels(3);
	if (ui->pushButton_3->text() == utils->BUTTON3_CAPTION_NORMAL) {
		ui->pushButton_3->setText(utils->BUTTON3_CAPTION_PRESSED);
		ui->label->setSelectionMode(MA_DELETE_SUBJECT);
	} else if (ui->pushButton_3->text() == utils->BUTTON3_CAPTION_PRESSED) {
		ui->pushButton_3->setText(utils->BUTTON3_CAPTION_NORMAL);
		ui->label->setSelectionMode(MA_SELECT_NONE);
	} else {
		qDebug("wtf");
	}
}

/* Clear selections */
void MainWindow::button4_clicked()
{
	cv::Mat img;
	AppUtils* utils = AppUtils::inst();

	resetLabels(4);
	if (ui->pushButton_4->text() == utils->BUTTON4_CAPTION_NORMAL) {
		ui->pushButton_4->setText(utils->BUTTON4_CAPTION_PRESSED);
	} else if (ui->pushButton_4->text() == utils->BUTTON4_CAPTION_PRESSED) {
		ui->pushButton_4->setText(utils->BUTTON4_CAPTION_NORMAL);
	} else {
		qDebug("wtf");
	}
	emit clearSelections();
}

/* Start/stop motors */
void MainWindow::button5_clicked()
{
	cv::Mat img;
	AppUtils* utils = AppUtils::inst();

	resetLabels(5);
	if (ui->pushButton_5->text() == utils->BUTTON5_CAPTION_NORMAL) {
		ui->pushButton_5->setText(utils->BUTTON5_CAPTION_PRESSED);
		emit startMotors();
	} else if (ui->pushButton_5->text() == utils->BUTTON5_CAPTION_PRESSED) {
		ui->pushButton_5->setText(utils->BUTTON5_CAPTION_NORMAL);
		emit stopMotors();
	} else {
		qDebug("wtf");
	}
}

void MainWindow::buttonWarningPushed()
{
	emit sendWarning(ui->time_slider->value());
}

void MainWindow::doCompositing(QImage& image)
{
	AppUtils *utils = AppUtils::inst();
	QRect selection;
	bool selecting;
	QPainter painter;

	selecting = ui->label->tryGetSelection(selection);
	if (selecting == true) {
		painter.begin(&image);
		painter.setPen(QPen(QColor(Qt::black)));
		painter.setBrush(QBrush(QColor(Qt::black), Qt::NoBrush));
		painter.drawRect(selection);
		painter.end();
	}
}

void MainWindow::frameReady(QImage image)
{
	AppUtils *utils = AppUtils::inst();
	QRect selection;
	bool selecting;

	doCompositing(image);

	ui->label->setPixmap(QPixmap::fromImage(image, Qt::AutoColor));
}

void MainWindow::newPhotogRect(QRect rect)
{
	AppUtils *utils = AppUtils::inst();

	qDebug("newPhotogRect() called");
	ui->pushButton_1->setText(utils->BUTTON1_CAPTION_NORMAL);
	ui->label->setSelectionMode(MA_SELECT_NONE);
}

void MainWindow::newSubjectRect(QRect rect)
{
	AppUtils *utils = AppUtils::inst();

	qDebug("newSubjectRect() called");
	ui->pushButton_2->setText(utils->BUTTON2_CAPTION_NORMAL);
	ui->label->setSelectionMode(MA_SELECT_NONE);
}

void MainWindow::deleteSubjectRect(QRect rect)
{
	AppUtils *utils = AppUtils::inst();

	qDebug("deleteSubjectRect() called");
	ui->pushButton_3->setText(utils->BUTTON3_CAPTION_NORMAL);
	ui->label->setSelectionMode(MA_SELECT_NONE);
}

QObject* MainWindow::getColdSlider()
{
	return ui->cold_slider;
}

QObject* MainWindow::getWarmSlider()
{
	return ui->warm_slider;
}

QObject* MainWindow::getBrightSlider()
{
	return ui->bright_slider;
}

QObject* MainWindow::getWarningButton()
{
	return ui->warningButton;
}

void MainWindow::updateStatus(int status) {
	AppUtils *utils = AppUtils::inst();

	printf("status = %i\n", status);
	if (status == utils->NOT_CONNECTED) {
		ui->warm_slider->setEnabled(false);
		ui->cold_slider->setEnabled(false);
		ui->bright_slider->setEnabled(false);
		ui->warningButton->setEnabled(false);
		ui->btfeedback->setText("no connection");
	}
	else {
		ui->btfeedback->setText("connected");
		ui->warm_slider->setEnabled(true);
		ui->cold_slider->setEnabled(true);
		ui->bright_slider->setEnabled(true);
		ui->warningButton->setEnabled(true);
		emit updateBluetoothHandle();
	}

}

void MainWindow::updateBtPanel() {
	printf("bright = %d \n", ui->bright_slider->value());
	emit signalUpdateBtPanel(ui->bright_slider->value(),
                        ui->warm_slider->value(),
                        ui->cold_slider->value());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QMessageBox::StandardButton question =
		QMessageBox::question(
				this,
				"Quit",
				tr("Are you sure?\n"),
				QMessageBox::No | QMessageBox::Yes,
				QMessageBox::Yes
		);

	if (question == QMessageBox::Yes) {
		emit closeApplication();
		event->accept();
	} else {
		event->ignore();
	}
}

void MainWindow::setPreview()
{
	ui->preview->setText(QString("No\npreview\navailable"));
}
