#pragma once

#include "TargetCollection.h"
#include <QObject>

class MotorCommunicator : public QObject {
	Q_OBJECT;
	int fuzzySocket;
	bool motorsEnabled;

public:
	MotorCommunicator();
	~MotorCommunicator();
public slots:
	void updateTargets(QObject*);
	void startMotors();
	void stopMotors();
};

