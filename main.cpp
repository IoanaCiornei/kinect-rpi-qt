#include "MainWindow.h"
#include <stdio.h>
#include <QApplication>
#include <QObject>
#include <QString>
#include <QTimer>

#include "MotorCommunicator.h"
#include "BluetoothCommunicator.h"
#include "VideoThread.h"
#include "AppUtils.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	BluetoothCommunicator *bt;
	MotorCommunicator *mc;
	VideoThread *vt;
	AppUtils *utils;
	MainWindow *w;
	QTimer *timer;

	utils = AppUtils::inst();
	bt    = new BluetoothCommunicator(utils->BLUETOOTH_CHANNEL);
	mc    = new MotorCommunicator();
	vt    = new VideoThread();
	w     = new MainWindow();
	timer = new QTimer();

	w->show();

	/* MouseArea to VideoThread */
	QObject::connect(vt, SIGNAL(frameReady(QImage)),
	                 w,  SLOT(frameReady(QImage)));
	QObject::connect(w->getLabel(), SIGNAL(newPhotogRect(QRect)),
	                 vt, SLOT(newPhotogRect(QRect)));
	QObject::connect(w->getLabel(), SIGNAL(newSubjectRect(QRect)),
	                 vt, SLOT(newSubjectRect(QRect)));
	QObject::connect(w->getLabel(), SIGNAL(deleteSubjectRect(QRect)),
	                 vt, SLOT(deleteSubjectRect(QRect)));

	/* MainWindow to VideoThread */
	QObject::connect(w, SIGNAL(clearSelections()),
	                 vt, SLOT(clearSelections()));

	/* MotorCommunicator hooks */
	QObject::connect(w, SIGNAL(startMotors()),
	                 mc, SLOT(startMotors()));
	QObject::connect(w, SIGNAL(stopMotors()),
	                 mc, SLOT(stopMotors()));
	QObject::connect(vt, SIGNAL(updateTargets(QObject*)),
	                 mc, SLOT(updateTargets(QObject*)));

	/* Bluetooth hooks */
	QObject::connect(bt, SIGNAL(updateStatus(int)),
                     w,  SLOT(updateStatus(int)));
	QObject::connect(timer, SIGNAL(timeout()),
                     w,  SLOT(updateBtPanel()));
	QObject::connect(timer, SIGNAL(timeout()),
                     bt, SLOT(getConnectionStatus()));

	QObject::connect(w,  SIGNAL(sendWarning(int)),
                     bt, SLOT(sendWarning(int)));
	QObject::connect(w,  SIGNAL(signalUpdateBtPanel(int, int, int)),
                     bt, SLOT(updatePanel(int, int, int)));
	QObject::connect(w,  SIGNAL(updateBluetoothHandle()),
                     bt, SLOT(updateBluetoothHandle()));
	QObject::connect(w,  SIGNAL(closeApplication()),
                     bt, SLOT(closeApplication()));
	timer->start(300);

	vt->connectToDevice();
	vt->start();

	return a.exec();
}

